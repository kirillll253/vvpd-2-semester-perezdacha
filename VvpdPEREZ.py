def ComplexReader():
    while True:
        comple = input()
        if comple.count('/') == 1:
            comp_1 = comple[0:(comple.index('/'))]
            comple = comple + ' '
            comp_2 = comple[(comple.index('/') + 1):-1]
            if comp_1.isdigit() and comp_2.isdigit():
                return int(comp_1)/int(comp_2)
            else:
                print('некорректный ввод')
        else:
            print('Некорректный ввод')


check = True
while check:
    a = input('Выберите действие с комплексными числами: 1-сложение, '
              '2-вычитание a-b, 3-умножение, 4-проверка равенства,'
              '5-возведение в степень, 6-вычисление корня степени ')
    print(a)
    if a.isdigit():
        a = int(a)
        if 0 < a < 7:
            check = False
        else:
            print('Введите целое число от 1 до 6')
    else:
        print('Введите целое число от 1 до 6')

print('введите действительную часть числа a в формате x/y, где x и y-целые числа')
a1 = ComplexReader()
print('введите мнимую часть числа a в формате x/y, где x и y-целые числа')
a2 = ComplexReader()
a_complex = complex(a1, a2)
if 0 < a < 5:
    print('введите действительную часть числа b в формате x/y, где x и y-целые числа')
    b1 = ComplexReader()
    print('введите мнимую часть числа b в формате x/y, где x и y-целые числа')
    b2 = ComplexReader()
    b_complex = complex(b1, b2)
else:
    while True:
        n = input('введите степень')
        if n.isdigit():
            n = int(n)
            if a == 6 and n < 2:
                print('Степень корня не может быть ниже 2')  # По крайней мере, мне так сказал гугл
            else:
                break
        else:
            print('Некорректный ввод')
if a == 1:
    print(a_complex, '+', b_complex, '=', a_complex+b_complex)
elif a == 2:
    print(a_complex, '-', b_complex, '=', a_complex - b_complex)
elif a == 3:
    print(a_complex, '*', b_complex, '=', a_complex * b_complex)
elif a == 4:
    if a_complex == b_complex:
        print('Данные числа равны')
    else:
        print('Данные числа не равны')
elif a == 5:
    print(a_complex, '^', n, pow(a_complex, n))
elif a == 6:
    print('корень', n, 'степени из', a_complex, '=', pow(a_complex, 1/n))

